package com.kamil.service;

public class EmptyOrderException extends RuntimeException {

    public EmptyOrderException(){
        super("Empty order");
    }
}
