package com.kamil.model.drink;

public class IceLatte extends Coffee {
    private boolean iceCubes;

    public IceLatte(boolean syrup, boolean soyMilk, boolean iceCubes) {
        super(8, syrup, soyMilk);
        this.iceCubes = iceCubes;
    }

    public boolean isIceCubes(){
        return iceCubes;
    }
}
