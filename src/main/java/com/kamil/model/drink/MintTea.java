package com.kamil.model.drink;

public class MintTea extends Tea {
    private boolean longBrewed;

    public MintTea(boolean syrup, int sugar, boolean longBrewed) {
        super(5, syrup, sugar);
        this.longBrewed = longBrewed;
    }

    public boolean isLongBrewed() {
        return longBrewed;
    }
}
