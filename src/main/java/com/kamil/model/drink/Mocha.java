package com.kamil.model.drink;

public class Mocha extends Coffee {
    private boolean seasoning;

    public Mocha(boolean syrup, boolean soyMilk, boolean seasoning) {
        super(4, syrup, soyMilk);
        this.seasoning = seasoning;
    }

    public boolean isSeasoning(){
        return seasoning;
    }


}
