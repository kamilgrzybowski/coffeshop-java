package com.kamil.model;

import java.util.List;

public class Order {
    private int orderId;
    private List<Product> products;
    private List<Extra> extras;
    private int orderPrice;

    public Order(int orderId, List<Product> products, List<Extra> extras, int orderPrice) {
        this.orderId = orderId;
        this.products = products;
        this.extras = extras;
        this.orderPrice = orderPrice;
    }

    public int getOrderId() {
        return orderId;
    }

    public List<Product> getProducts() {
        return products;
    }

    public List<Extra> getExtras() {
        return extras;
    }

    public int getOrderPrice() {
        return orderPrice;
    }
}
