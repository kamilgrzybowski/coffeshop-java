package com.kamil.model.food;

import com.kamil.model.Product;

public class Food extends Product {
    private boolean cream;

     public Food(){

     }

    public Food(int price, boolean cream) {
        super(price);
        this.cream = cream;
    }

    public boolean isCream() {
        return cream;
    }
}
