package com.kamil.model.food;

public class Cheesecake extends Cake {
    private boolean fruits;

    public Cheesecake(boolean cream, boolean chocklate, boolean fruits) {
        super(6, cream, chocklate);
        this.fruits = fruits;
    }

    public boolean isFruits() {
        return fruits;
    }
}
