package com.kamil.model.food;

public class Icecream extends Food {
    private boolean wafers;

    public Icecream() {
    }

    public Icecream(int price, boolean cream, boolean wafers) {
        super(price, cream);
        this.wafers = wafers;
    }

    public boolean getWafers() {
        return wafers;
    }


}
