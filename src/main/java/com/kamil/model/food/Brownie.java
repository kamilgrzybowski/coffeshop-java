package com.kamil.model.food;

public class Brownie extends Cake {
    private boolean chutney;

    public Brownie(boolean cream, boolean chocklate, boolean chutney) {
        super(7, cream, chocklate);
        this.chutney = chutney;
    }

    public boolean isChutney() {
        return chutney;
    }
}
