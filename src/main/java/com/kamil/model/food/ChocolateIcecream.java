package com.kamil.model.food;

public class ChocolateIcecream extends Icecream {

    public ChocolateIcecream(boolean cream, boolean wafers) {
        super(2, cream, wafers);
    }

}
