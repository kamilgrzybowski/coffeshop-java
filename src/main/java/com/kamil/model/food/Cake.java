package com.kamil.model.food;

public class Cake extends Food {
    private boolean chocklate;

    public Cake() {
    }

    public Cake(int price, boolean cream, boolean chocklate) {
        super(price, cream);
        this.chocklate = chocklate;
    }

    public boolean isChocklate() {
        return chocklate;
    }
}
