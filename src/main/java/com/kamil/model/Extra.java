package com.kamil.model;

public enum Extra {
    SUGAR(1),
    ESPRESSO(2),
    COCA(3),
    CREAM(2),
    WAFER(1);

    private int price;

    Extra(int price){
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
